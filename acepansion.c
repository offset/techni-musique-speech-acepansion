/****** technimusique-speech.acepansion/background **************************
*
* DESCRIPTION
*  This plugin provides TMPI speech synthetizer emulation to ACE CPC
*  Emulator.
*
* HISTORY
*
*****************************************************************************
*
*/

#include <clib/alib_protos.h>

#include <proto/exec.h>

#define USE_INLINE_API
#include <acepansion/lib_header.h>
#include <libraries/acepansion_plugin.h>

#include "acepansion.h"
#include "mea8000.h"
#define CATCOMP_NUMBERS
#include "generated/locale_strings.h" /* catalog string ids */



struct PluginData
{
    // Plugin generic part (do NEVER put anything before this!)
    struct ACEpansionPlugin common;

    // TMPI speech synthetizer specifics
    struct mea8000_t *mea;
};



/*
** Private function which is called to initialize commons
** just after the library was loaded into memory and prior
** to any other API call.
**
** This is the good place to open our required libraries.
*/
BOOL InitResources(VOID)
{
    return TRUE;
}



/*
** Private function which is called to free commons
** just before the library is expurged from memory
**
** This is the good place to close our required libraries.
*/
VOID FreeResources(VOID)
{
}



/****** technimusique-speech.acepansion/CreatePlugin ************************
*
* NAME
*   CreatePlugin -- (V1) -- GUI
*
* SYNOPSIS
*   struct ACEpansionPlugin * CreatePlugin(CONST_STRPTR *toolTypes, struct SignalSemaphore *sema)
*
* FUNCTION
*   Create the plugin data structure and configure how it will be handled by
*   ACE. This function is usually called at ACE startup.
*
* INPUTS
*   *toolType
*     A pointer to a NULL terminated array of CONST_STRPTR containing the
*     tooltypes provided with the plugin library.
*   *sema
*     Pointer to a semaphore to use to protect access to custom data from
*     ACEpansionPlugin when accessed from subtasks or within your PrefsWindow.
*
* RESULT
*     A pointer to an allocated and initialized plugin data structure.
*
* SEE ALSO
*   DeletePlugin
*   ActivatePlugin
*   GetPrefsPlugin
*
*****************************************************************************
*
*/

ACEPANSION_API
struct ACEpansionPlugin * CreatePlugin(UNUSED CONST_STRPTR *toolTypes, UNUSED struct SignalSemaphore *sema)
{
    // We are allocating an extended plugin structure to add our custom stuff
    struct PluginData *myPlugin = (struct PluginData*)AllocVec(sizeof(struct PluginData), MEMF_PUBLIC|MEMF_CLEAR);

    if(myPlugin)
    {
        // Initialize plugin generic part
        myPlugin->common.ap_APIVersion     = API_VERSION;
        myPlugin->common.ap_Flags          = ACE_FLAGSF_ACTIVE_AUDIO      // We need the audio API
                                           | ACE_FLAGSF_ACTIVE_WRITEIO    // We need the write IO API
                                           | ACE_FLAGSF_ACTIVE_READIO     // We need the read IO API
                                           | ACE_FLAGSF_ACTIVE_EMULATE;   // We need the emulate API
        myPlugin->common.ap_Title          = GetString(MSG_TITLE);
        myPlugin->common.ap_HelpFileName   = LIBNAME".guide";
        myPlugin->common.ap_ToggleMenuName = GetString(MSG_MENU_TOGGLE);
        myPlugin->common.ap_PrefsMenuName  = NULL;
        myPlugin->common.ap_PrefsWindow    = NULL;

        // Initialize plugin specifics
        myPlugin->mea = mea8000_create(3840000);

        if(myPlugin->mea == NULL)
        {
            DeletePlugin((struct ACEpansionPlugin *)myPlugin);
            myPlugin = NULL;
        }
    }

    return (struct ACEpansionPlugin *)myPlugin;
}



/****** technimusique-speech.acepansion/DeletePlugin ************************
*
* NAME
*   DeletePlugin -- (V1) -- GUI
*
* SYNOPSIS
*   VOID DeletePlugin(struct ACEpansionPlugin *plugin)
*
* FUNCTION
*   Delete a plugin data structure. This function is usually called when ACE
*   is exiting.
*
* INPUTS
*   *plugin
*     Pointer to the plugin data structure to delete.
*
* RESULT
*
* SEE ALSO
*   CreatePlugin
*   ActivatePlugin
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID DeletePlugin(struct ACEpansionPlugin *plugin)
{
    struct PluginData *myPlugin = (struct PluginData *)plugin;

    mea8000_destroy(myPlugin->mea);
    FreeVec(myPlugin);
}



/****** technimusique-speech.acepansion/ActivatePlugin **********************
*
* NAME
*   ActivatePlugin -- (V1) -- GUI
*
* SYNOPSIS
*   BOOL ActivatePlugin(struct ACEpansionPlugin *plugin, BOOL status)
*
* FUNCTION
*   This function is called everytime ACE wants to activate or disactivate your plugin.
*   While you are disactivated, no other function (except Delete) will the called by ACE.
*   Please note that a plugin is always disactivated by ACE prior to DeletePlugin().
*
* INPUTS
*   activate
*     New activation status requested by ACE.
*
* RESULT
*   Actual activation status (you may fail at activation for some reason, but you
*   should never fail at disactivation request).
*
* SEE ALSO
*   CreatePlugin
*   DeletePlugin
*
*****************************************************************************
*
*/

ACEPANSION_API
BOOL ActivatePlugin(struct ACEpansionPlugin *plugin, BOOL activate)
{
    struct PluginData *myPlugin = (struct PluginData *)plugin;

    if(activate)
    {
        mea8000_reset(myPlugin->mea);
    }

    return activate;
}



/****** technimusique-speech.acepansion/GetPrefsPlugin **********************
*
* NAME
*   GetPrefsPlugin -- (V5) -- GUI
*
* SYNOPSIS
*   STRPTR * GetPrefsPlugin(struct ACEpansionPlugin *plugin)
*
* FUNCTION
*   Function that is used by ACE to know about the current preferences of the
*   plugins in order to save them.
*
* INPUTS
*   *plugin
*     Pointer to the plugin data structure to use.
*   pool
*     Memory pool to use to allocate the tooltypes.
*
* RESULT
*   Pointer to a NULL terminated string array containing the tooltypes or NULL
*   when no tooltypes.
*
* SEE ALSO
*   CreatePlugin
*
*****************************************************************************
*
*/

ACEPANSION_API
STRPTR * GetPrefsPlugin(UNUSED struct ACEpansionPlugin *plugin, UNUSED APTR pool)
{
    return NULL;
}



/****** technimusique-speech.acepansion/Reset *******************************
*
* NAME
*   Reset -- (V1) -- EMU
*
* SYNOPSIS
*   VOID Reset(struct ACEpansionPlugin *plugin)
*
* FUNCTION
*   Function that is called everytime ACE is issuing a bus reset.
*
* INPUTS
*   *plugin
*     Pointer to the plugin data structure to use.
*
* RESULT
*
* SEE ALSO
*   Emulate
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID Reset(UNUSED struct ACEpansionPlugin *plugin)
{
    struct PluginData *myPlugin = (struct PluginData *)plugin;

    mea8000_reset(myPlugin->mea);
}



/****** technimusique-speech.acepansion/Emulate *****************************
*
* NAME
*   Emulate -- (V1) -- EMU
*
* SYNOPSIS
*   VOID Emulate(struct ACEpansionPlugin *plugin, ULONG *signals)
*
* FUNCTION
*   Function that is called at each microsecond of the emulation step inside ACE.
*   Note: Implement this function only if is it really required for your plugin because it
*   is obviously CPU intensive.
*
* INPUTS
*   *plugin
*     Pointer to the plugin data structure to use.
*   *signals
*     Pointer to the I/O signals from the expansion port you can use or alter
*     depending on what you are emulating.
*     The signals bits can be decoded using the following flags:
*       ACE_SIGNALF_INT -- (V1)
*         Maskable interrupt; corresponding to /INT signal from CPC expansion
*         port.
*       ACE_SIGNALF_NMI -- (V3)
*         Non-maskable interrupt; corresponding to /NMI signal from CPC
*         expansion port.
*       ACE_SIGNALF_LPEN -- (V4)
*         Light pen trigger; corresponding to /LPEN signal from CPC expansion
*         port.
*       ACE_SIGNALF_BUS_RESET -- (V6)
*         Bus reset trigger; correspond to /BUSRESET signal from CPC expansion
*         port.
*       ACE_SIGNALF_NOT_READY -- (V6)
*         Not ready trigger; correspond to READY signal from CPC expansion port.
*
* RESULT
*
* SEE ALSO
*   Reset
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID Emulate(UNUSED struct ACEpansionPlugin *plugin, UNUSED ULONG *signals)
{
    struct PluginData *myPlugin = (struct PluginData *)plugin;

    mea8000_tick(myPlugin->mea);
}



/****** technimusique-speech.acepansion/WriteIO *****************************
*
* NAME
*   WriteIO -- (V1) -- EMU
*
* SYNOPSIS
*   VOID WriteIO(struct ACEpansionPlugin *plugin, USHORT port, UBYTE value)
*
* FUNCTION
*   Function that is called everytime Z80 is performing a I/O port write operation.
*   Note: Implement this function only if is it really required for your plugin because it
*   is obviously CPU intensive.
*
* INPUTS
*   port
*     Z80 port on with the I/O write operation was issued.
*   value
*     Value that was issued on the port.
*
* RESULT
*
* SEE ALSO
*   ReadIO
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID WriteIO(struct ACEpansionPlugin *plugin, USHORT port, UBYTE value)
{
    struct PluginData *myPlugin = (struct PluginData *)plugin;

    if((port & 0x00ff) == 0xff)
    {
        mea8000_write_command(myPlugin->mea, value);
    }
    else if((port & 0x00ff) == 0xfe)
    {
        mea8000_write_data(myPlugin->mea, value);
    }
}



/****** technimusique-speech.acepansion/ReadIO ******************************
*
* NAME
*   ReadIO -- (V1) -- EMU
*
* SYNOPSIS
*   VOID ReadIO(struct ACEpansionPlugin *plugin, USHORT port, UBYTE *value)
*
* FUNCTION
*   Function that is called everytime Z80 is performing a I/O port read operation.
*   Note: Implement this function only if is it really required for your plugin because it
*   is obviously CPU intensive.
*
* INPUTS
*   port
*     Z80 port on with the I/O read operation was issued.
*   *value
*     Value to be returned.
*
* RESULT
*
* SEE ALSO
*   WriteIO
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID ReadIO(struct ACEpansionPlugin *plugin, USHORT port, UBYTE *value)
{
    struct PluginData *myPlugin = (struct PluginData *)plugin;

    if((port & 0xff) == 0xff)
    {
        *value = mea8000_read_status(myPlugin->mea);
    }
}



/****** technimusique-speech.acepansion/WriteMem ****************************
*
* NAME
*  WriteMem -- (V6) -- EMU
*
* SYNOPSIS
*  BOOL WriteMem(struct ACEpansionPlugin *plugin, USHORT address, UBYTE value)
*
* FUNCTION
*  Function that is called everytime Z80 is performing a memory write
*  operation.
*
* INPUTS
*  *plugin
*    Pointer to the plugin data structure to use.
*  address
*    Z80 address on which the memory write operation was issued.
*  value
*    Value that has to be written.
*
* RESULT
*  TRUE if the plugins actually cougth the memory write operation so that
*  internal memory will be left unchanged (/RAMDIS is set to 0).
*
* NOTES
*  Implement this function only if is it really required for your plugin
*  because it is obviously CPU intensive.
*
* SEE ALSO
*  ReadMem()
*
*****************************************************************************
*
*/

ACEPANSION_API
BOOL WriteMem(UNUSED struct ACEpansionPlugin *plugin, UNUSED USHORT address, UNUSED UBYTE value)
{
    return FALSE;
}



/****** technimusique-speech.acepansion/ReadMem *****************************
*
* NAME
*  ReadMem -- (V6) -- EMU
*
* SYNOPSIS
*  VOID ReadMem(struct ACEpansionPlugin *plugin, USHORT address, UBYTE *value, BOOL opcodeFetch) -- (V7)
*  BOOL ReadMem(struct ACEpansionPlugin *plugin, USHORT address, UBYTE *value, BOOL opcode) ** DEPRECATED in V7 **

* FUNCTION
*  Function that is called everytime Z80 is performing a memory read
*  operation.
*
* INPUTS
*  *plugin
*    Pointer to the plugin data structure to use.
*  address
*    Z80 address on which memory read operation was issued.
*  *value
*    Value to be returned.
*    Starting with V7, it contains by default the value that might be read by
*    the CPC if the plugin won't catch the memory read, so that a plugin can
*    only snif the bus.
*  opcodeFetch
*    Boolean which is TRUE when the memory read operation is related to
*    Z80 opcode fetching (/M1 signal). If FALSE, then this is a regular
*    read operation during opcode execution.
*
* RESULT
*  ** DEPRECATED in V7 **
*  TRUE if the plugins actually cougth the memory read operation so that
*  internal memory won't be read (both /ROMDIS and /RAMDIS are set to 0).
*
* NOTES
*  Implement this function only if is it really required for your plugin
*  because it is obviously CPU intensive.
*
* SEE ALSO
*  WriteMem()
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID ReadMem(UNUSED struct ACEpansionPlugin *plugin, UNUSED USHORT address, UNUSED UBYTE *value, UNUSED BOOL opcodeFetch)
{
}



/****** technimusique-speech.acepansion/GetAudio ****************************
*
* NAME
*   GetAudio -- (V1) -- EMU
*
* SYNOPSIS
*   VOID GetAudio(struct ACEpansionPlugin *plugin, SHORT *leftSample, SHORT *rightSample)
*
* FUNCTION
*   Function to be used when your plugin needs to mix an audio output with the one
*   from ACE.
*   It is called at each PSG emulation step, which means at 125KHz.
*
* INPUTS
*   *leftSample
*     Pointer to store the left audio sample to be mixed with ACE audio.
*   *rightSample
*     Pointer to store the right audio sample to be mixed with ACE audio.
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID GetAudio(struct ACEpansionPlugin *plugin, SHORT *leftSample, SHORT *rightSample)
{
    struct PluginData *myPlugin = (struct PluginData *)plugin;

    *leftSample = *rightSample = mea8000_get_audio(myPlugin->mea);
}



/****** technimusique-speech.acepansion/Printer *****************************
*
* NAME
*   Printer -- (V1) -- EMU
*
* SYNOPSIS
*   VOID WritePrinter(struct ACEpansionPlugin *plugin, UBYTE data, BOOL strobe, BOOL *busy)
*
* FUNCTION
*   Function that is called everytime the CPC is accessing the printer port.
*
* INPUTS
*   data
*     8 bits written value (note that CPC is limited to 7 bits values, only Amstrad Plus
*     can actually provide the 8th bit).
*   strobe
*     Status of the /STROBE signal from the printer port.
*   *busy
*     Busy signal value that can be altered.
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID Printer(UNUSED struct ACEpansionPlugin *plugin, UNUSED UBYTE data, UNUSED BOOL strobe, UNUSED BOOL *busy)
{
}



/****** technimusique-speech.acepansion/Joystick ****************************
*
* NAME
*   Joystick -- (V2) -- EMU
*
* SYNOPSIS
*   VOID Joystick(struct ACEpansionPlugin *plugin, BOOL com1, BOOL com2, UBYTE *ioData)
*
* FUNCTION
*   Function which is called everytime the CPC is accessing the joystick port.
*   Please note that the joystick port in R/W but you cannot know if the CPC
*   is actually reading or writing to it.
*
* INPUTS
*   com1
*     If com1 is active then the CPC is requesting access to joystick 0.
*   com2
*     If com2 is active then the CPC is requesting access to joystick 1.
*   *ioData
*     If the CPC is reading the joystick port (regular case) ioData will be set
*     to 0xff and you are supposed to set to 0 the bits corresponding to the
*     requested joystick (you should use the ACE_IODATAF_JOYSTICK_XXX defines).
*     Please note the additional ACE_IODATAF_JOYSTICK_PAUSE which is not
*     existing on CPC but that you could use to map the play/pause button of
*     a joystick used in ACE on the 'P' key of the keyboard (or the GX4000
*     pause button).
*     If the CPC is writing the joystick port the ioData will contain the
*     written value to the port and modifying it will have not effect (on a
*     real CPC it could simply destroy the PSG chipset!).
*
* RESULT
    To emulate the analog joystick port of the Amstrad Plus and the GX-4000,
    this function should be used together with AnalogInput().
*
* SEE ALSO
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID Joystick(UNUSED struct ACEpansionPlugin *plugin, UNUSED BOOL com1, UNUSED BOOL com2, UNUSED UBYTE *ioData)
{
}



/****** technimusique-speech.acepansion/AnalogInput *************************
*
* NAME
*   AnalogInput -- (V7) -- EMU
*
* SYNOPSIS
*   VOID AnalogInput(struct ACEpansionPlugin *plugin, UBYTE channel, UBYTE *value)
*
* FUNCTION
*   Function which is called everytime the CPC is accessing the analog joystick
*   port of the Amstrad Plus or the GX-4000.
*
* INPUTS
*   *plugin
*     Pointer to the plugin data structure to use.
*   channel
*     Analog channel number (from 0 to 3).
*   *value
*     Pointer where to store the 6-bit wide value of the data on the channel.
*
* RESULT
*
* NOTES
*   To emulate the analog joystick port of the Amstrad Plus and the GX-4000,
*   this function should be used together with Joystick() which is providing
*   fire buttons (they are shared between both analog and digital joystick
*   ports).
*
* SEE ALSO
*   Joystick()
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID AnalogInput(UNUSED struct ACEpansionPlugin *plugin, UNUSED UBYTE channel, UNUSED UBYTE *value)
{
}



/****** technimusique-speech.acepansion/AcknowledgeInterrupt ****************
*
* NAME
*   AcknowledgeInterrupt -- (V1) -- EMU
*
* SYNOPSIS
*   VOID AcknowledgeInterrupt(struct ACEpansionPlugin *plugin, UBYTE *ivr)
*
* FUNCTION
*   Function which is called everytime the Z80 is entering an interrupt.
*
* INPUTS
*   *ivr
*     IVR could be set to the desired value (Interrupt Vector Register).
*
* RESULT
*
* SEE ALSO
*   ReturnInterrupt
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID AcknowledgeInterrupt(UNUSED struct ACEpansionPlugin *plugin, UNUSED UBYTE *ivr)
{
}



/****** technimusique-speech.acepansion/ReturnInterrupt *********************
*
* NAME
*   ReturnInterrupt -- (V3) -- EMU
*
* SYNOPSIS
*   VOID ReturnInterrupt(struct ACEpansionPlugin *plugin)
*
* FUNCTION
*   Function which is called everytime the Z80 is returning from an interrupt
*   using RETI instruction.
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*   AcknowledgeInterrupt
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID ReturnInterrupt(UNUSED struct ACEpansionPlugin *plugin)
{
}



/****** technimusique-speech.acepansion/Cursor ******************************
*
* NAME
*   Cursor -- (V1) -- EMU
*
* SYNOPSIS
*   VOID Cursor(struct ACEpansionPlugin *plugin)
*
* FUNCTION
*   Function which is called everytime the cursor signal from the CRTC is
*   active.
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID Cursor(UNUSED struct ACEpansionPlugin *plugin)
{
}



/****** technimusique-speech.acepansion/HostGamepadList *********************
*
* NAME
*   HostGamepadList -- (V7) -- GUI

* SYNOPSIS
*   VOID HostGamepadList(struct ACEpansionPlugin *plugin, STRPTR *list)
*
* FUNCTION
*   This function is called everytime a gamepad in plugged or unplugged from
*   the host so that the plugin can know about the available devices. Index
*   from HostGamepadEvent() will refer to this list.
*
* INPUTS
*   *plugin
*     Pointer to the plugin data structure to use.
*   list
*     NULL terminated array of strings containing the human readable names of
*     the plugged gamepads, the first one being the localized string for "No
*     joystick". Please note that the contents of this list remains valid
*     until the function is invoked again, so that you can use it safely in
*     your code without duplication.
*
* RESULT
*
* NOTES
*
* SEE ALSO
*   HostGamepadList()
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID HostGamepadList(UNUSED struct ACEpansionPlugin *plugin, UNUSED STRPTR *list)
{
}



/****** technimusique-speech.acepansion/HostGamepadEvent ********************
*
* NAME
*   HostGamepadEvent -- (V7) -- GUI
*
* SYNOPSIS
*   VOID HostGamepadEvent(struct ACEpansionPlugin *plugin, UBYTE index, USHORT buttons, BYTE ns, BYTE ew, BYTE lx, BYTE ly, BYTE rx, BYTE ry)
*
* FUNCTION
*   This function is called everytime the state of a host's gamepad state is
*   changing. It let you handle both analogic and digital devices.
*
* INPUTS
*   *plugin
*     Pointer to the plugin data structure to use.
*   index
*     Index of the gamepad in gamepad list.
*   buttons
*     State of the gamepad buttons.
*     Buttons bits can be decoded using the ACE_HOSTGAMEPADF_BUTTON_XXX flags.
*   ns
*     Horizontal position of directional buttons from -128 to +127.
*   ew
*     Vertical position of directional buttons from -128 to +127.
*   lx
*     Horizontal position of left stick from -128 to +127.
*   ly
*     Vertical position of left stick from -128 to +127.
*   rx
*     Horizontal position of right stick from -128 to +127.
*   ry
*     Vertical position of right stick from -128 to +127.
*
* RESULT
*
* NOTES
*
* SEE ALSO
*   HostGamepadList()
*   libraries/acepansion_plugin.h
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID HostGamepadEvent(UNUSED struct ACEpansionPlugin *plugin, UNUSED UBYTE index, UNUSED USHORT buttons, UNUSED BYTE ns, UNUSED BYTE ew, UNUSED BYTE lx, UNUSED BYTE ly, UNUSED BYTE rx, UNUSED BYTE ry)
{
}



/****** technimusique-speech.acepansion/HostMouseEvent **********************
*
* NAME
*   HostMouseEvent -- (V7) -- GUI

* SYNOPSIS
*   VOID HostMouseEvent(struct ACEpansionPlugin *plugin, UBYTE buttons, SHORT deltaX, SHORT deltaY)

* FUNCTION
*   Function that is called everytime a host's mouse event occured

* INPUTS
*   *plugin
*     Pointer to the plugin data structure to use.
*   buttons
*     State of the mouse buttons.
*     The mouse buttons bits can be decoded using the following flags:
*       ACE_HOSTMOUSEF_BUTTON_MAIN
*         State of main mouse button.
*       ACE_HOSTMOUSEF_BUTTON_SECOND
*         State of second mouse button.
*       ACE_HOSTMOUSEF_BUTTON_MIDDLE
*         State of middle mouse button.
*   deltaX
*     Horizontal mouse move until previous event.
*   deltaY
*     Vertical mouse move until previous event.
*
* RESULT
*
* NOTES
*
* SEE ALSO
      libraries/acepansion_plugin.h
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID HostMouseEvent(UNUSED struct ACEpansionPlugin *plugin, UNUSED UBYTE buttons, UNUSED SHORT deltaX, UNUSED SHORT deltaY)
{
}



/****** technimusique-speech.acepansion/HostLightDeviceDiodePulse ***********
*
* NAME
*   HostLightDeviceDiodePulse -- (V7) -- EMU
*
* SYNOPSIS
*   VOID HostLightDeviceDiodePulse(struct ACEpansionPlugin *plugin)
*
* FUNCTION
*   This function is called at each photo-diode pulse, when the beam from
*   the screen monitor is just in front of the light device photo-diode.
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*   HostLightDeviceButton
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID HostLightDeviceDiodePulse(UNUSED struct ACEpansionPlugin *plugin)
{
}



/****** technimusique-speech.acepansion/HostLightDeviceButton ***************
*
* NAME
*   HostLightDeviceButton -- (V7) -- GUI
*
* SYNOPSIS
*   VOID HostLightDeviceButton(struct ACEpansionPlugin *plugin, BOOL pressed)
*
* FUNCTION
*   This function is called everytime the state of the light device button
*   is changing.
*
* INPUTS
*   pressed
*     New state of the light device button.
*
* RESULT
*
* NOTE
*   When a light device plugin is activated, ACE is mapping this button on
*   mouse main button, so that on-screen click will emulate light device
*   button.
*
* SEE ALSO
*   HostLightDeviceDiodePulse
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID HostLightDeviceButton(UNUSED struct ACEpansionPlugin *plugin, UNUSED BOOL pressed)
{
}

