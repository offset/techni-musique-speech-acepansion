/*
** Converted to plain C without MESS dependencies
** by Philippe 'OffseT' Rimauro
** (disabled req part which will have to be handled manually)
*/

/**********************************************************************

  Copyright (C) Antoine Min� 2006

  Philips / Signetics MEA 8000 emulation.

  The MEA 8000 is a speech synthesis chip.
  The French company TMPI (Techni-musique & parole informatique) provided
  speech extensions for several 8-bit computers (Thomson, Amstrad, Oric).
  It was quite popular in France because of its ability to spell 'u'
  (unlike the more widespread SPO 296 chip).

  The synthesis is based on a 4-formant model.
  First, an initial sawtooth noise signal is generated.
  The signal passes through a cascade of 4 filters of increasing frequency.
  Each filter is a second order digital filter with a programmable
  frequency and bandwidth.
  All parameters, including filter parameters, are smoothly interpolated
  for the duration of a frame (8ms, 16ms, 32ms, or 64 ms).

  TODO:
  - REQ output pin
  - optimize mea8000_compute_sample
  - should we accept new frames in slow-stop mode ?

**********************************************************************/

#include <clib/alib_protos.h>
#include <clib/debug_protos.h>
#include <proto/exec.h>
#include <string.h>
#include <math.h>

#include "mea8000.h"

extern struct Library *SysBase;



/******************* utilitiy function and macros ********************/



#define VERBOSE 0

/* define to use DOUBLE instead of int (slow but useful for debugging) */
#undef FLOAT_MODE


/* table amplitude [-QUANT,QUANT] */
#define QUANT 512

/* filter coefficients from frequencies */
#define TABLE_LEN 3600

/* noise generator table */
#define NOISE_LEN 8192

#ifdef __HAIKU__
#include <stdio.h>
#define LOG(x) do { if (VERBOSE) printf x; } while (0)
#else
#define LOG(x) do { if (VERBOSE) kprintf x; } while (0)
#endif

/* digital filters work at 8 kHz */
#define F0 8096

/* filtered output is supersampled x 8 */
#define SUPERSAMPLING 8

// useful macros to deal with bit shuffling encryptions
#define BIT(x,n) (((x)>>(n))&1)



/************************ private structures *************************/



/* finite machine state controling frames */
enum mea8000_state
{
    MEA8000_STOPPED,    /* nothing to do, timer disabled */
    MEA8000_WAIT_FIRST, /* received pitch, wait for first full trame, timer disabled */
    MEA8000_STARTED,    /* playing a frame, timer on */
    MEA8000_SLOWING,    /* repating last frame with decreasing amplitude, timer on */
};


struct filter_t
{
#ifdef FLOAT_MODE
    DOUBLE fm, last_fm;         /* frequency, in Hz */
    DOUBLE bw, last_bw;         /* band-width, in Hz */
    DOUBLE output, last_output; /* filter state */
#else
    USHORT fm, last_fm;
    USHORT bw, last_bw;
    LONG  output, last_output;
#endif
};


struct mea8000_t
{
    /* state */
    enum mea8000_state state; /* current state */

    UBYTE buf[4]; /* store 4 consecutive data to form a frame info */
    UBYTE bufpos; /* new byte to write in frame info buffer */

    UBYTE cont; /* if no data 0=stop 1=repeat last frame */
    UBYTE roe;  /* enable req output, now unimplemented */

    USHORT framelength;  /* in samples */
    USHORT framepos;     /* in samples */
    USHORT framelog;     /* log2 of framelength */

    SHORT lastsample, sample; /* output samples are interpolated */

    ULONG phi; /* absolute phase for frequency / noise generator */

    struct filter_t f[4]; /* filters */

    USHORT last_ampl, ampl;    /* amplitude * 1000 */
    USHORT last_pitch, pitch;  /* pitch of sawtooth signal, in Hz */
    UBYTE noise;

    int cos_table[TABLE_LEN];  /* fm => cos coefficient */
    int exp_table[TABLE_LEN];  /* bw => exp coefficient */
    int exp2_table[TABLE_LEN]; /* bw => 2*exp coefficient */
    int noise_table[NOISE_LEN];

    int tick;
    int tick_ref;

    SHORT dac;
};



/************************* quantization tables ***********************/



/* frequency, in Hz */

static const int fm1_table[32] =
{
    150,  162,  174,  188,  202,  217,  233,  250,
    267,  286,  305,  325,  346,  368,  391,  415,
    440,  466,  494,  523,  554,  587,  622,  659,
    698,  740,  784,  830,  880,  932,  988, 1047
};

static const int fm2_table[32] =
{
    440,  466,  494,  523,  554,  587,  622,  659,
    698,  740,  784,  830,  880,  932,  988, 1047,
    1100, 1179, 1254, 1337, 1428, 1528, 1639, 1761,
    1897, 2047, 2214, 2400, 2609, 2842, 3105, 3400
};

static const int fm3_table[8] =
{
    1179, 1337, 1528, 1761, 2047, 2400, 2842, 3400
};

static const int fm4_table[1] = { 3500 };



/* bandwidth, in Hz */
static const int bw_table[4] = { 726, 309, 125, 50 };



/* amplitude * 1000 */
static const int ampl_table[16] =
{
    0,   8,  11,  16,  22,  31,  44,   62,
    88, 125, 177, 250, 354, 500, 707, 1000
};



/* pitch increment, in Hz / 8 ms */
static const int pi_table[32] =
{
    0, 1,  2,  3,  4,  5,  6,  7,
    8, 9, 10, 11, 12, 13, 14, 15,
    0 /* noise */, -15, -14, -13, -12, -11, -10, -9,
    -8, -7, -6, -5, -4, -3, -2, -1
};



/************************* private declaration ***********************/
                           


static int accept_byte(struct mea8000_t *mea);

static VOID init_tables(struct mea8000_t *mea);

#ifndef FLOAT_MODE
static int interp(struct mea8000_t *mea, USHORT org, USHORT dst);
static int filter_step(struct mea8000_t *mea, int i, int input);
static int noise_gen(struct mea8000_t *mea);
static int freq_gen(struct mea8000_t *mea);
#else
static DOUBLE interp(struct mea8000_t *mea, DOUBLE org, DOUBLE dst);
static DOUBLE filter_step(struct mea8000_t *mea, int i, DOUBLE input);
static DOUBLE noise_gen(struct mea8000_t *mea);
static DOUBLE freq_gen(struct mea8000_t *mea);
#endif
static int compute_sample(struct mea8000_t *mea);

static VOID shift_frame(struct mea8000_t *mea);
static VOID decode_frame(struct mea8000_t *mea);
static VOID start_frame(struct mea8000_t *mea);
static VOID stop_frame(struct mea8000_t *mea);
static VOID compute_frame(struct mea8000_t *mea);



//-------------------------------------------------
//  device_start - device-specific startup
//-------------------------------------------------

struct mea8000_t * mea8000_create(ULONG clock_hz)
{
    struct mea8000_t *mea = (struct mea8000_t*)AllocVec(sizeof(struct mea8000_t), MEMF_PUBLIC|MEMF_CLEAR);

    if(mea)
    {
        mea->tick_ref = 1000000 * SUPERSAMPLING / clock_hz * F0;

        LOG(("mea8000_create %d\n", mea->tick_ref));

        init_tables(mea);
    }

    return mea;
}


//-------------------------------------------------
//  device_destroy - device-specific destruction
//-------------------------------------------------

VOID mea8000_destroy(struct mea8000_t *mea)
{
    FreeVec(mea);
}


//-------------------------------------------------
//  device_reset - device-specific reset
//-------------------------------------------------

VOID mea8000_reset(struct mea8000_t *mea)
{
    LOG(("mea8000_reset\n"));

    mea->phi = 0;
    mea->cont = 0;
    mea->roe = 0;
    mea->state = MEA8000_STOPPED;
#if 0
    update_req(mea);
#endif
    for (int i = 0; i < 4; i++)
    {
        mea->f[i].last_output = 0;
        mea->f[i].output = 0;
    }
    mea->dac = 0;
}


//-------------------------------------------------
//  device_tick - device tick (1 microsecond)
//-------------------------------------------------

VOID mea8000_tick(struct mea8000_t *mea)
{
    mea->tick += 1000;

    while(mea->tick > mea->tick_ref)
    {
        mea->tick -= mea->tick_ref;

        compute_frame(mea);
    }
}


//-------------------------------------------------
//  device_get_audio - device-specific get_audio
//-------------------------------------------------

SHORT mea8000_get_audio(struct mea8000_t *mea)
{
    return mea->dac;
}



/************************** CPU interface ****************************/


UBYTE mea8000_read_status(struct mea8000_t *mea)
{
    /* ready to accept next frame */
#if 0
    LOG(("mea8000_r ready=%i\n", accept_byte()));
#endif
    return accept_byte(mea) << 7;
}

VOID mea8000_write_command(struct mea8000_t *mea, UBYTE data)
{
    int stop = BIT(data, 4);

    if (data & 8)
    mea->cont = BIT(data, 2);

    if (data & 2)
        mea->roe = BIT(data, 0);

    if (stop)
        stop_frame(mea);

    LOG(("mea8000_w command %02X stop=%i cont=%i roe=%i\n", data, stop, mea->cont, mea->roe));

#if 0
    update_req(mea);
#endif
}

VOID mea8000_write_data(struct mea8000_t *mea, UBYTE data)
{
    if (mea->state == MEA8000_STOPPED)
    {
        /* got pitch byte before first frame */
        mea->pitch = 2 * data;
        LOG(("mea8000_w pitch %i\n", mea->pitch));
        mea->state = MEA8000_WAIT_FIRST;
        mea->bufpos = 0;
    }
    else if (mea->bufpos == 4)
    {
        /* overflow */
        LOG(("mea8000_w data overflow %02X\n", data));
    }
    else
    {
        /* enqueue frame byte */
        LOG(("mea8000_w data %02X in frame pos %i\n", data, mea->bufpos));
        mea->buf[mea->bufpos] = data;
        mea->bufpos++;
        if (mea->bufpos == 4 && mea->state == MEA8000_WAIT_FIRST)
        {
            /* fade-in first frame */
            int old_pitch = mea->pitch;
            mea->last_pitch = old_pitch;
            decode_frame(mea);
            shift_frame(mea);
            mea->last_pitch = old_pitch;
            mea->ampl = 0;
            start_frame(mea);
            mea->state = MEA8000_STARTED;
        }
    }
#if 0
    update_req(mea);
#endif
}



/***************************** REQ **********************************/


static int accept_byte(struct mea8000_t *mea)
{
    return mea->state == MEA8000_STOPPED || mea->state == MEA8000_WAIT_FIRST || (mea->state == MEA8000_STARTED && mea->bufpos < 4);
}

#if 0
static VOID update_req(struct mea8000_t *mea)
{
    // actually, req pulses less than 3us for each new byte,
    // it goes back up if there space left in the buffer, or stays low if the
    // buffer contains a complete frame and the CPU needs to wait for the next
    // frame end to compose a new frame.
    write_req(accept_byte(mea));
}
#endif



/*********************** sound generation ***************************/


/* precompute tables */
static VOID init_tables(struct mea8000_t *mea)
{
    for (int i = 0; i < TABLE_LEN; i++)
    {
        DOUBLE f = (DOUBLE)i / F0;
        mea->cos_table[i]  = 2. * cos(2. * M_PI * f) * QUANT;
        mea->exp_table[i]  = exp(-M_PI * f) * QUANT;
        mea->exp2_table[i] = exp(-2 * M_PI * f) * QUANT;
    }
    for (int i = 0; i < NOISE_LEN; i++)
        mea->noise_table[i] = RangeRand((2 * QUANT) - 1) - QUANT;
}


#ifndef FLOAT_MODE /* USHORT version */



/* linear interpolation */
static int interp(struct mea8000_t *mea, USHORT org, USHORT dst)
{
    return org + (((dst - org) * mea->framepos) >> mea->framelog);
}


/* apply second order digital filter, sampling at F0 */
static int filter_step(struct mea8000_t *mea, int i, int input)
{
    /* frequency */
    int fm = interp(mea, mea->f[i].last_fm, mea->f[i].fm);
    /* bandwidth */
    int bw = interp(mea, mea->f[i].last_bw, mea->f[i].bw);
    /* filter coefficients */
    int b = (mea->cos_table[fm] * mea->exp_table[bw]) / QUANT;
    int c = mea->exp2_table[bw];
    /* transfer function */
    int next_output = input + (b * mea->f[i].output - c * mea->f[i].last_output) / QUANT;
    mea->f[i].last_output = mea->f[i].output;
    mea->f[i].output = next_output;
    return next_output;
}


/* random waveform, in [-QUANT,QUANT] */
static int noise_gen(struct mea8000_t *mea)
{
    mea->phi = (mea->phi + 1) % NOISE_LEN;
    return mea->noise_table[mea->phi];
}


/* sawtooth waveform at F0, in [-QUANT,QUANT] */
static int freq_gen(struct mea8000_t *mea)
{
    int pitch = interp(mea, mea->last_pitch, mea->pitch);
    mea->phi = (mea->phi + pitch) % F0;
    return ((mea->phi % F0) * QUANT * 2) / F0 - QUANT;
}


/* sample in [-32768,32767], at F0 */
static int compute_sample(struct mea8000_t *mea)
{
    int out;
    int ampl = interp(mea, mea->last_ampl, mea->ampl);

    if (mea->noise)
        out = noise_gen(mea);
    else
        out = freq_gen(mea);

    out *= ampl / 32;

    for (int i = 0; i < 4; i++)
        out = filter_step(mea, i, out);

    if (out > 32767)
        out = 32767;
    if (out < -32767)
        out = -32767;
    return out;
}



#else /* float version */



/* linear interpolation */
static DOUBLE interp(struct mea8000_t *mea, DOUBLE org, DOUBLE dst)
{
    return org + ((dst - org) * mea->framepos) / mea->framelength;
}


/* apply second order digital filter, sampling at F0 */
static DOUBLE filter_step(struct mea8000_t *mea, int i, DOUBLE input)
{
    DOUBLE fm = interp(mea, mea->f[i].last_fm, mea->f[i].fm);
    DOUBLE bw = interp(mea, mea->f[i].last_bw, mea->f[i].bw);
    DOUBLE b = 2. * cos(2. * M_PI * fm / F0);
    DOUBLE c = -exp(-M_PI * bw / F0);
    DOUBLE next_output = input - c * (b * mea->f[i].output + c * mea->f[i].last_output);
    mea->f[i].last_output = mea->f[i].output;
    mea->f[i].output = next_output;
    return next_output;
}


/* noise, in [-1,1] */
static DOUBLE noise_gen(struct mea8000_t *mea)
{
    mea->phi++;
    return (DOUBLE) mea->noise_table[mea->phi % NOISE_LEN] / QUANT;
}



/* sawtooth waveform at F0, in [-1,1] */
static DOUBLE freq_gen(struct mea8000_t *mea)
{
    int pitch = interp(mea, mea->last_pitch, mea->pitch);
    mea->phi += pitch;
    return (DOUBLE) (mea->phi % F0) / (F0 / 2.) - 1.;
}


/* sample in [-32767,32767], at F0 */
static int compute_sample(struct mea8000_t *mea)
{
    DOUBLE out;
    DOUBLE ampl = interp(mea, 8. * mea->last_ampl, 8. * mea->ampl);

    if (mea->noise)
        out = noise_gen(mea);
    else
        out = freq_gen(mea);

    out *= ampl;

    for (int i = 0; i < 4; i++)
    {
        out = filter_step(mea, i, out);
    }

    if (out > 32767)
        out = 32767;
    if (out < -32767)
        out = -32767;
    return out;
}


#endif


/*********************** frame management ***************************/



/* shift frame parameters from current to last */
static VOID shift_frame(struct mea8000_t *mea)
{
    mea->last_pitch = mea->pitch;
    for (int i = 0; i < 4; i++)
    {
        mea->f[i].last_bw = mea->f[i].bw;
        mea->f[i].last_fm = mea->f[i].fm;
    }
    mea->last_ampl = mea->ampl;
}



/* decode fields from buffer to current frame */
static VOID decode_frame(struct mea8000_t *mea)
{
    int fd = (mea->buf[3] >> 5) & 3; /* 0=8ms, 1=16ms, 2=32ms, 3=64ms */
    int pi = pi_table[mea->buf[3] & 0x1f] << fd;
    mea->noise = (mea->buf[3] & 0x1f) == 16;
    mea->pitch = mea->last_pitch + pi;
    mea->f[0].bw = bw_table[mea->buf[0] >> 6];
    mea->f[1].bw = bw_table[(mea->buf[0] >> 4) & 3];
    mea->f[2].bw = bw_table[(mea->buf[0] >> 2) & 3];
    mea->f[3].bw = bw_table[mea->buf[0] & 3];
    mea->f[3].fm = fm4_table[0];
    mea->f[2].fm = fm3_table[mea->buf[1] >> 5];
    mea->f[1].fm = fm2_table[mea->buf[1] & 0x1f];
    mea->f[0].fm = fm1_table[mea->buf[2] >> 3];
    mea->ampl = ampl_table[((mea->buf[2] & 7) << 1) | (mea->buf[3] >> 7)];
    mea->framelog = fd + 6 /* 64 samples / ms */ + 3;
    mea->framelength = 1 << mea->framelog;
    mea->bufpos = 0;
#ifdef FLOAT_MODE
    LOG(("mea800_decode_frame: pitch=%i noise=%i  fm1=%gHz bw1=%gHz  fm2=%gHz bw2=%gHz  fm3=%gHz bw3=%gHz  fm4=%gHz bw4=%gHz  ampl=%g fd=%ims\n",
            mea->pitch, mea->noise,
            mea->f[0].fm, mea->f[0].bw, mea->f[1].fm, mea->f[1].bw,
            mea->f[2].fm, mea->f[2].bw, mea->f[3].fm, mea->f[3].bw,
            mea->ampl/1000., 8 << fd));
#else
    LOG(("mea800_decode_frame: pitch=%i noise=%i  fm1=%iHz bw1=%iHz  fm2=%iHz bw2=%iHz  fm3=%iHz bw3=%iHz  fm4=%iHz bw4=%iHz  ampl=%g fd=%ims\n",
            mea->pitch, mea->noise,
            mea->f[0].fm, mea->f[0].bw, mea->f[1].fm, mea->f[1].bw,
            mea->f[2].fm, mea->f[2].bw, mea->f[3].fm, mea->f[3].bw,
            mea->ampl/1000., 8 << fd));
#endif
}



static VOID start_frame(struct mea8000_t *mea)
{
    /* enter or stay in active mode */
    mea->framepos = 0;
}



static VOID stop_frame(struct mea8000_t *mea)
{
    /* enter stop mode */
    mea->state = MEA8000_STOPPED;
    mea->dac = 0;
}



/* next sample in frame, sampling at 64 kHz */
static VOID compute_frame(struct mea8000_t *mea)
{
    int pos = mea->framepos % SUPERSAMPLING;

    if (!pos)
    {
        /* sample is really computed only every 8-th time */
        mea->lastsample = mea->sample;
        mea->sample = compute_sample(mea);
        mea->dac = mea->lastsample;
    }
    else
    {
        /* other samples are simply interpolated */
        int sample = mea->lastsample + ((pos * (mea->sample-mea->lastsample)) / SUPERSAMPLING);
        mea->dac = sample;
    }

    mea->framepos++;
    if (mea->framepos >= mea->framelength)
    {
        shift_frame(mea);
        /* end of frame */
        if (mea->bufpos == 4)
        {
            /* we have a successor */
            LOG(("mea8000_timer_expire: new frame\n"));
            decode_frame(mea);
            start_frame(mea);
        }
        else if (mea->cont)
        {
            /* repeat mode */
            LOG(("mea8000_timer_expire: repeat frame\n"));
            start_frame(mea);
        }
        /* slow stop */
        else if (mea->state == MEA8000_STARTED)
        {
            mea->ampl = 0;
            LOG(("mea8000_timer_expire: fade frame\n"));
            start_frame(mea);
            mea->state = MEA8000_SLOWING;
        }
        else if (mea->state == MEA8000_SLOWING)
        {
            LOG(("mea8000_timer_expire: stop frame\n"));
            stop_frame(mea);
        }
#if 0
        update_req(mea);
#endif
    }
}

