/*
** Converted to plain C without MESS dependencies
** by Philippe 'OffseT' Rimauro
*/

/**********************************************************************

  Copyright (C) Antoine Min� 2006

  Philips MEA 8000 emulation.

**********************************************************************/

#ifndef __MEA8000_H__
#define __MEA8000_H__


#ifndef EXEC_TYPES_H
#include <exec/types.h>
#endif


/* ---------------------------------------------------------------- */
/*  Actual public API.                                              */
/* ---------------------------------------------------------------- */
struct mea8000_t;

struct mea8000_t * mea8000_create(ULONG clock_hz);
VOID mea8000_destroy(struct mea8000_t *mea);

VOID mea8000_tick(struct mea8000_t *mea);
VOID mea8000_reset(struct mea8000_t *mea);
SHORT mea8000_get_audio(struct mea8000_t *mea);

UBYTE mea8000_read_status(struct mea8000_t *mea);

VOID mea8000_write_command(struct mea8000_t *mea, UBYTE data);
VOID mea8000_write_data(struct mea8000_t *mea, UBYTE data);


#endif
